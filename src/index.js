// server
const express     = require('express')
const app         = express()
const debug       = require('debug')('logo-api')
const compression = require('compression')
const helmet      = require('helmet');
// svg drawing
const window      = require('svgdom')
const SVG         = require('svg.js')(window)

const document = window.document
const draw = SVG(document.documentElement)
const {Logo, defaultAnimationInStatic} = require('matiere-vive-logo');
const {getRandomM, getRandomV} = require('matiere-vive-logo/src/letters.js');
const textsymbol = draw.symbol();
const matierevive = require('./matierevive.svg.js');
const sharp = require('sharp');
const maxwidth = 2000;
const PORT = process.env.PORT || 4578;

const animationIn = (line, container) => {
  defaultAnimationInStatic(MVLogo, line, container);
}
var MVLogo = new Logo(32,draw, {
  sync: false,
  hasText: false,
  // text: MATIEREVIVE,
  // functions
  animationIn: animationIn,
});

// const MATIEREVIVE = require('./matiere-vive.svg');

app.use(compression());
app.use(helmet());

app.use('/width/:width', function (req, res, next) {
  const width = parseInt(req.params.width);
  MVLogo.clear();
  MVLogo.removeText();
  MVLogo.setDimensions(width>maxwidth ? maxwidth : width);
  MVLogo.drawGrid();
  MVLogo.generateNewLetters(getRandomM(), getRandomV());
  res.type('image/svg+xml')
  next();
})

app.use('*/withtext', function (req, res, next) {
  MVLogo.addText(matierevive);
  next();
})

app.use('*/scheme/:scheme', function (req, res, next) {
  MVLogo.switchColor(req.params.scheme);
  MVLogo.generateNewLetters(getRandomM(), getRandomV());
  next();
})

app.use('*/format/:format', function (req, res, next) {
  const {format} = req.params;
  if (format=='svg') {
    res.type('image/svg+xml');
    next();
    return;
  }
  const svgLogo = MVLogo.print();
  const image = sharp(Buffer.from(svgLogo.svg()));
  // console.log(image.toBuffer({info:{format:'jpeg'}}));
  switch (format) {
    case 'jpg':
    case 'jpeg':
      // image.background({r: 255, g: 255, b: 255, alpha: 1});
      image.flatten().background('#ffffff').toBuffer({info:{format:'jpeg'}}, (err, data, info) => {
        if (err) {
          debug(`Error generating jpg: ${err}`);
        } else {
          res.type('image/jpg');
          res.send(data);
        }
      });
      break;
    case 'png':
      image.toBuffer({info:{format:'png'}}, (err, data, info) => {
        if (err) {
          debug(`Error generating png: ${err}`);
        } else {
          res.type('image/png');
          res.send(data);
        }
      });
      break;
  }
})
app.get('*', function(req, res) {
  const svgLogo = MVLogo.print();
  res.send(svgLogo.svg());
})

app.listen(PORT, function () {
  debug(`Example app listening on http://localhost:${PORT} !`)
})
